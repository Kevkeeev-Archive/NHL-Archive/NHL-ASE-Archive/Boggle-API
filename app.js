var createError		= require('http-errors');
var express			= require('express');
var path			= require('path');
var cookieParser	= require('cookie-parser');
var bodyParser		= require('body-parser');
var logger			= require('morgan');
var mysql			= require('mysql'); 	// db
var fs				= require('fs');		// read words
var readline		= require('readline');	// read words

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.static(path.join(__dirname, 'public')));


// HEADERS
app.use(function(req, res, next) {
	res.setHeader('Access-Control-Allow-Origin', req.headers.origin );
	next();
});
// HEADERS

// WORDS
var words = [], wordfile = path.join(__dirname, 'public', 'words.txt');
readline.createInterface({
	input: fs.createReadStream(wordfile),
	terminal: false
}).on('line', function(line) {
	words.push(line);
}).on('close', function(){
	console.log('Counted words: ' + words.length);
});
app.use(function(req, res, next){ req.words = words; next(); });
// WORDS

var boggle = require('./routes/boggle'); app.use('/', boggle);


// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
